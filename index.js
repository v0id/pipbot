const TelegramBot = require('node-telegram-bot-api');
const token = process.env.BOT_TOKEN;
const bot = new TelegramBot(token, { polling: true });

const explicaciones = [
    {
        nombre: 'TERF',
        triggers: ['terf'],
        explicacion: `Siglas de "Trans-Exlusionary Radical Feminist" (en inglés). Feminista radical transfóbica.`,
    },
    {
        nombre: 'Markdown',
        triggers: ['markdown', 'md'],
        explicacion: `Formato de archivo de texto que te deja agregar links, negrita, tablas y otras cosas por encima de texto plano. Jekyll usa este formato por defecto.`,
    },
    {
        nombre: 'Como agregar terminos al bot',
        triggers: ['agregar terminos'],
        explicacion: `Mandar PR a https://0xacab.org/v0idifier/pipbot (o consultar a \`@v0idifier\`)`,
    },
    {
        nombre: 'Inkscape',
        triggers: ['inkscape'],
        explicacion: `Es un programa vectorial de comunicación visual, permite crear imágenes con texto y exportarlo en varios formatos de imagén/documento como pdf, png o svg.`,
    },
];

bot.onText(/^\/explicar (.+)/, (msg, match) => {
    const chatId = msg.chat.id;
    const palabra = match[1];
    const explicacion = explicaciones.find(
        ({ triggers }) => triggers.indexOf(palabra.toLowerCase()) !== -1,
    );

    if (!explicacion) {
        return bot.sendMessage(
            chatId,
            'No tengo una definición para esa palabra.',
            { reply_to_message_id: msg.message_id },
        );
    }

    const replyTo =
        msg.reply_to_message
            ? msg.reply_to_message.message_id
            : msg.message_id;

    bot.sendMessage(
        chatId,
        `${explicacion.nombre}: ${explicacion.explicacion}`,
        { reply_to_message_id: replyTo },
    );
});

